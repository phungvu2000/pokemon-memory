export const shuffed = (list) => {
  return list.sort(() => Math.random() - 0.5)
}
